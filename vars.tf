variable "environment" {
  description = "environment where this resource is used"
  type        = string
}

variable "domain_name" {
  description = "domain of the email service"
  type        = string
}

variable "receive_email_prefix" {
  description = ""
  type        = string
}

variable "success_topic_arn" {
  description = ""
  type        = string
}

variable "error_topic_arn" {
  description = ""
  type        = string
}

variable "bucket_name" {
  description = "name of the bucket where the incoming mails are stored"
  type        = string
}

variable "receipt_rule_set" {
  description = "name of the default receipt rule set"
  type = string
}

data "aws_route53_zone" "domain_zone" {
  name = var.domain_name
}

locals {
  receive_email_address = "${var.receive_email_prefix}@${var.domain_name}"
}