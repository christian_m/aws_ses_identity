module "dkim_verification_records" {
  count       = 3
  source      = "git::git@bitbucket.org:christian_m/aws_route53_resource_record.git?ref=v1.0"
  environment = var.environment
  zone_id     = data.aws_route53_zone.domain_zone.zone_id
  record      = {
    name   = "${element(aws_ses_domain_dkim.default.dkim_tokens, count.index)}._domainkey.${aws_ses_domain_dkim.default.domain}"
    type   = "CNAME"
    ttl    = "600"
    values = ["${element(aws_ses_domain_dkim.default.dkim_tokens, count.index)}.dkim.amazonses.com",]
  }
  allow_overwrite = true
}

module "ses_mx_record" {
  source      = "git::git@bitbucket.org:christian_m/aws_route53_resource_record.git?ref=v1.0"
  environment = var.environment
  zone_id     = data.aws_route53_zone.domain_zone.zone_id
  record      = {
    name   = var.domain_name
    type   = "MX"
    ttl    = "600"
    values = ["10 inbound-smtp.eu-west-1.amazonaws.com"]
  }
  allow_overwrite = true
}

module "ses_spf_record" {
  source      = "git::git@bitbucket.org:christian_m/aws_route53_resource_record.git?ref=v1.0"
  environment = var.environment
  zone_id     = data.aws_route53_zone.domain_zone.zone_id
  record      = {
    name   = var.domain_name
    type   = "TXT"
    ttl    = "600"
    values = ["v=spf1 include:amazonses.com -all"]
  }
  allow_overwrite = true
}