resource "aws_ses_domain_identity" "default" {
  domain = var.domain_name
}

resource "aws_ses_domain_dkim" "default" {
  domain = aws_ses_domain_identity.default.domain
}

resource "aws_ses_domain_identity_verification" "default" {
  domain = aws_ses_domain_identity.default.id

  depends_on = [module.dkim_verification_records]
}

resource "aws_ses_email_identity" "default" {
  email = local.receive_email_address

  depends_on = [aws_ses_receipt_rule.success, aws_ses_domain_identity_verification.default]
}

resource "aws_ses_active_receipt_rule_set" "default" {
  rule_set_name = var.receipt_rule_set

  depends_on = [
    aws_ses_receipt_rule.success
  ]
}

resource "aws_ses_receipt_rule" "success" {
  name          = "default_receipt_rule_${var.receive_email_prefix}"
  rule_set_name = var.receipt_rule_set

  recipients = [
    local.receive_email_address
  ]

  enabled      = true
  scan_enabled = true

  s3_action {
    bucket_name = var.bucket_name
    topic_arn   = var.success_topic_arn
    position    = 1
  }

  stop_action {
    scope    = "RuleSet"
    position = 2
  }
}

resource "aws_ses_configuration_set" "default" {
  name = "ses_configuration_set_${var.receive_email_prefix}"
}

resource "aws_ses_event_destination" "error" {
  name                   = "error_sns_destination_${var.receive_email_prefix}"
  configuration_set_name = aws_ses_configuration_set.default.name
  enabled                = true

  matching_types = [
    "reject",
    "send",
  ]

  sns_destination {
    topic_arn = var.error_topic_arn
  }
}

resource "aws_ses_event_destination" "ses_cloudwatch" {
  name                   = "ses_cloudwatch_destination_${var.receive_email_prefix}"
  configuration_set_name = aws_ses_configuration_set.default.name
  enabled                = true

  matching_types = [
    "reject",
    "send",
  ]

  cloudwatch_destination {
    default_value  = "default"
    dimension_name = "dimension"
    value_source   = "emailHeader"
  }
}